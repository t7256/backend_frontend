package main

import (
	"backend/database"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
func main() {

	fmt.Println("Starting...")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("/")
	})

	http.HandleFunc("/getAllTempData", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("/GetAllTempData")
		data := database.GetAllTempData()
		enableCors(&w)
		jsonData, err := json.Marshal(&data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(w, "%s", jsonData)
	})

	http.HandleFunc("/getLatestTempData", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("/getLatestTempData")
		data := database.GetLatestTempData()
		enableCors(&w)
		jsonData, err := json.Marshal(&data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(w, "%s", jsonData)
	})

	fmt.Printf("Starting server at port 8080\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
