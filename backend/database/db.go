package database

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	host   = "localhost"
	port   = 5432
	user   = "postgres"
	dbname = "office_conditions"
)

var err error
var db *sql.DB

type Tempdata struct {
	Temperature_id int    `json:"temperature_id"`
	Temperature    int    `json:"temperature"`
	Date           string `json:"date"`
	Time           string `json:"time"`
	Room           string `json:"room"`
}

func ConnectDb() {
	connStr := fmt.Sprintf("user=%s dbname=%s host=%s sslmode=disable port=%d", user, dbname, host, port)

	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nSuccessfully connected to database!\n")
}

func InsertTempData(temp int, date string, time string, room string) {
	ConnectDb()
	sqlStatement := `INSERT INTO public.temperature (temperature, date, time, room)
	VALUES($1, $2, $3, $4)`
	_, err = db.Exec(sqlStatement, temp, date, time, room)

	if err != nil {
		panic(err)
	} else {
		fmt.Print("\nRow inserted succesfully!\n")
	}
}

func GetAllTempData() []Tempdata {
	ConnectDb()
	sqlStatement := `SELECT * FROM public.temperature`
	rows, err := db.Query(sqlStatement)

	if err != nil {
		panic(err)
	} else {
		fmt.Print("\nAll data sucesfully retrived\n")
	}

	defer rows.Close()
	defer db.Close()
	var tempdata []Tempdata
	for rows.Next() {
		var t Tempdata

		err = rows.Scan(&t.Temperature_id, &t.Temperature, &t.Date, &t.Time, &t.Room)
		if err != nil {
			panic(err)
		}

		tempdata = append(tempdata, t)
	}
	fmt.Printf("%+v\n", tempdata)
	return tempdata
}

func GetLatestTempData() []Tempdata {
	ConnectDb()
	var array []Tempdata
	sqlStatement := `SELECT * FROM public.temperature ORDER BY temperature_id DESC LIMIT 1`

	row := db.QueryRow(sqlStatement)

	defer db.Close()
	var t Tempdata
	switch err := row.Scan(&t.Temperature_id, &t.Temperature, &t.Date, &t.Time, &t.Room); err {
	case sql.ErrNoRows:
		fmt.Println("nothing found")
	case nil:
		array = append(array, t)
		fmt.Printf("%+v\n", array)
	default:
		panic(err)
	}
	return array
}
