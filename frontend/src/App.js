import './App.css';
import qrtech_logo from './qrtech-logo.png'
import TempData from './components/TempData';
import WeatherData from './components/WeatherData';

function App() {

  return (
    <div className='homepage'>
    <h1>WELCOME TO LASSEMAJAS</h1>
    <img className="qrtech-logo"src={qrtech_logo} alt="" width={"100vw"} height={"40vh"} />
    <TempData route={"getLatestTempData"}/> 
    <WeatherData/>
    {/* <script>document.getElementById(body).style.background-color = "blue"</script> */}
    </div>
  );
}


export default App;
