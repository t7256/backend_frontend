import React, { Component } from 'react';
import axios from 'axios'
import WeatherSymbol from './WeatherSymbol.js'

export class WeatherData extends Component {
  constructor(props) {
    super(props)

    this.state = {
      temperature: Number,
      weatherSymbol: Number
    }
  }
  componentDidMount() {
    this.getTemperature()
    setInterval(() => { this.getTemperature() }, 60000);
  }


  render() {
    const { temperature, weatherSymbol } = this.state
    return (
      <div
        style={{border : 'solid' , width: '300px', height : '300px'}}
      >
        <WeatherSymbol number={weatherSymbol}/>
        {
          <div >
            <h1>{Math.round(temperature)}º</h1>
          </div>
        }
        
      </div>
    )
  }

  async getTemperature() {

    try {
      const response = await axios.get(`https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/13.880026/lat/55.441169/data.json`)
      console.log(response.data);
      const temperature = response.data.timeSeries[0].parameters[10].values[0];
      const weatherSymbol = response.data.timeSeries[0].parameters[18].values[0]
      this.setState({ temperature, weatherSymbol })
    }
    catch (error) {
      console.log(error)
    }
  }
}


export default WeatherData;
