import React from 'react';

const Symbol = ({number}) => {
    const theImage = GetImage(number)
    return (
        <div>
            <img src={`/img/${theImage}`} alt="WeatherSymbol"></img>
        </div>
        )
};

const GetImage = (number) => {
    const image = `sym_${number}.png`
        return image
}

export default Symbol;
