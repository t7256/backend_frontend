import React, { Component } from 'react'
import sym_1 from './sym_1.png'
import gurra from './gurra.jpeg'


export class Symbol extends Component {
    constructor(props) {
        super(props)

        this.state = {
            image: String
        }
    }

    componentDidMount() {
        this.getImage()
      }

    render() {
        const { image } = this.state
        
        return (
            <div>
                {
                    <div>
                    <p>{image}</p>
                    <img src={image} alt="WeatherSymbol" />
                    </div>
                }
            </div>
        )
    }

    getImage() {
        const image = `sym_${this.props.number}`
        // const image = `./gurra.jpeg`
        console.log(image)
        this.setState({image: image})
    }
}

export default Symbol