import React, { Component } from 'react';
import axios from 'axios'

export class TempData extends Component {
    constructor(props) {
        super(props)

        this.state = {
            temperature: []
        }

    }

    componentDidMount() {
        this.getTemperature()
        setInterval(() => { this.getTemperature() }, 60000);
    }

    render() {
        const { temperature } = this.state
        return (
            <div>
                {
                    temperature.length ?
                        temperature.map(temperature =>
                            <div key={temperature.temperature_id}
                                style={{
                                    color: (15 < temperature.temperature && temperature.temperature < 25 ? 'green' :
                                        13 < temperature.temperature && temperature.temperature < 27 ? 'orange' :
                                            'red'), fontSize: "34px"
                                }}
                            >
                                {temperature.temperature}º
                            </div>
                        )
                        :
                        null
                }
            </div>

        )
    }

    async getTemperature() {

        try {
            const response = await axios.get(`http://localhost:8080/${this.props.route}`)
            console.log(response.data);
            this.setState({ temperature: response.data })
        }
        catch (error) {
            console.log(error)
        }
    }

}


export default TempData;
